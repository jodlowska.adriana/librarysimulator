package com.ada.librarysimulator;

public class Main {

    public static void main(String[] args) {

        Student student1 = new Student("Adam", "Mickiewicz");
        Student student2 = new Student("Hanna", "Montana");
        Lecturer lecturer1 = new Lecturer("Dawid", "Podsiadło");
        Lecturer lecturer2 = new Lecturer("Britney", "Spears");

        Library library1 = new Library();
        library1.addUserToLibrary(student1, student2, lecturer1, lecturer2);

        library1.printListOfUsers();

        Book book1a = new Book("Ogniem i mieczem", "H. Sienkiewicz");
        Book book1b = new Book("Ogniem i mieczem", "H. Sienkiewicz");

        library1.printListOfBooks();
        Book book1c = new Book("Ogniem i mieczem", "H. Sienkiewicz");
        Book book1d = new Book("Ogniem i mieczem", "H. Sienkiewicz");
        Book book2 = new Book("Tozsamosc Bourne'a", "R. Ludlum");
        Book book3 = new Book("Gra o tron", "George'a R.R. Martin");

        Magazine magazine1 = new Magazine("National Geographic", "01/2016");
        Magazine magazine2 = new Magazine("Avanti", "03/2019");

        library1.addItemToLibrary(book1a, book1b, book1c, book1d, book2, book3, magazine1, magazine2);
        library1.addItemToLibrary(book1a, book1b, magazine1, magazine2);

        library1.printListOfBooks();
        library1.printListOfMagazines();

        library1.rentItemToUser(book1a, lecturer1);
        library1.rentItemToUser(book1a, lecturer2);
        library1.rentItemToUser(book1a, lecturer1);
        library1.rentItemToUser(book1a, lecturer1);
        library1.rentItemToUser(book2, student1);
        library1.rentItemToUser(book3, student2);

        library1.printListOfBooks();
        library1.printListOfMagazines();
        library1.importItemsFromFile("./InputDataLibrary.csv");
        library1.exportUsersWithItemsToFile("./OutputDataLibrary.csv");
        library1.printListOfBooks();
        library1.printListOfMagazines();
    }
}
