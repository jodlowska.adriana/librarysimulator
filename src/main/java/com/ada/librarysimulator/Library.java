package com.ada.librarysimulator;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Library {

    private List<User> listOfUsers = new ArrayList<>();
    private Map<String, Item> mapOfItems = new HashMap<>();

    final static int LIMIT_OF_BOOKS_STUDENT = 4;
    final static int LIMIT_OF_BOOKS_LECTURER = 10;

    public Library() {

    }

    public void addUserToLibrary(User... user) {
        listOfUsers.addAll(Arrays.asList(user));
    }

    public void printListOfUsers() {
        listOfUsers.forEach(user -> user.print());
    }

    public void addItemToLibrary(Item... item) {
        for (Item i : item) {
            if (mapOfItems.get(i.title) == null) {
                mapOfItems.put(i.title, i);
            } else {
                Item existingItem = mapOfItems.get(i.title);
                existingItem.setAvailable(existingItem.getAvailable() + 1);
                existingItem.setQuantity(existingItem.getQuantity() + 1);
                mapOfItems.put(existingItem.title, existingItem);
            }
        }
    }

    public boolean rentItemToUser(Item item, User user) {
        Item borrowedItem = mapOfItems.get(item.title);
        if (borrowedItem == null) {
            System.out.println("This book is not in the library collection");
            return false;
        }

        if (borrowedItem.getAvailable() <= 0) {
            System.out.println("This book is currently unavailable");
            return false;
        }

        if (user.getType() == 'S') {
            if (user.getBorrowedItems().size() >= LIMIT_OF_BOOKS_STUDENT) {
                return false;
            } else {
                borrowedItem.setAvailable(borrowedItem.getAvailable() - 1);
                user.addBorrowedItems(item);
                System.out.println(borrowedItem.title + " borrowed to " + user.getFullName());
                return true;

            }
        } else if (user.getType() == 'L') {
            if (user.getBorrowedItems().size() >= LIMIT_OF_BOOKS_LECTURER) {
                return false;
            } else {
                borrowedItem.setAvailable(borrowedItem.getAvailable() - 1);
                user.addBorrowedItems(item);
                System.out.println(borrowedItem.title + " borrowed to " + user.getFullName());
                return true;
            }
        } else {
            System.out.println("Invalid user type");
            return false;
        }
    }

    public void printListOfMagazines() {
        Map<String, Item> magazines = mapOfItems.entrySet().stream().filter(item -> item.getValue() instanceof Magazine).collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
        magazines.forEach((title, magazine) -> System.out.println(magazine.toString() + magazine.getQuantity() + ";" + magazine.getAvailable() + ";"));
    }

    public void printListOfBooks() {
        Map<String, Item> books = mapOfItems.entrySet().stream().filter(item -> item.getValue() instanceof Book).collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
        books.forEach((title, magazine) -> System.out.println(magazine.toString() + magazine.getQuantity() + ";" + magazine.getAvailable() + ";"));
    }

    public void exportUsersWithItemsToFile(String csvFile) {
        List<String> dataLines = new ArrayList<>();

        for (User user : listOfUsers) {
            if (user.getBorrowedItems().isEmpty()) {
                continue;
            }

            StringJoiner userRecord = new StringJoiner("");
            userRecord.add(String.valueOf(user.getId()) + "[");
            for (Item item : user.getBorrowedItems()) {
                userRecord.add(item.title + "-");
                userRecord.add("AutorLubNumerMagazynu; ");
            }
            userRecord.add("]");
            dataLines.add(userRecord.toString());

        }

        File csvOutputFile = new File(csvFile);
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
                    .forEach(pw::println);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void importItemsFromFile(String csvFile) {
        List<List<String>> recordsList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                recordsList.add(Arrays.asList(values));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (List<String> records : recordsList) {
            char type = records.get(3).charAt(0);
            if (type == 'B') {
                Book book = new Book(records.get(0), records.get(1));
                for (int i = 0; i < Integer.parseInt(records.get(2)); i++) {
                    addItemToLibrary(book);
                }
            } else if (type == 'M') {
                Magazine magazine = new Magazine(records.get(0), records.get(1));
                for (int i = 0; i < Integer.parseInt(records.get(2)); i++) {
                    addItemToLibrary(magazine);
                }
            } else {
                System.out.println("Invalid user type");
            }
        }

    }
}
