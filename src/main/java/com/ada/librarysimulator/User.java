package com.ada.librarysimulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class User {

    final private int uid;
    final private String firstName;
    final private String lastName;
    final private char type;
    private List<Item> borrowedItems = new ArrayList<>();
    public static int nextUid = 1;

    public User(String firstName, String lastName, char type) {
        this.uid = nextUid++;
        this.firstName = firstName;
        this.lastName = lastName;
        this.type = type;
    }

    public void print() {
        System.out.println(firstName + ";" + lastName + ";" + uid + ";" + type);
    }

    @Override
    public String toString() {
        return firstName + ";" + lastName + ";" + uid + ";" + type;
    }

    public List<Item> getBorrowedItems() {
        return borrowedItems;
    }

    public void addBorrowedItems(Item... item) {
        borrowedItems.addAll(Arrays.asList(item));
    }

    public char getType() {
        return type;
    }

    public int getId() {
        return uid;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }
}