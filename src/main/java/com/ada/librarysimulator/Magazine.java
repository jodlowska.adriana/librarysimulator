package com.ada.librarysimulator;

public class Magazine extends Item {

    final private String number;

    public Magazine(String title, String number) {
        super(title);
        this.number = number;
    }

    @Override
    public String toString() {
        return title + ";" + number + ";";
    }
}
