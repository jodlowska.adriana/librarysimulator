package com.ada.librarysimulator;

public class Item {

    final protected String title;
    private int quantity;
    private int available;

    public Item(String title) {
        this.title = title;
        this.quantity = 1;
        this.available = 1;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
