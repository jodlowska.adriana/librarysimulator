package com.ada.librarysimulator;

public class Book extends Item {

    final private String author;

    public Book(String title, String author) {
        super(title);
        this.author = author;
    }

    @Override
    public String toString() {
        return title + ";" + author + ";";
    }
}
