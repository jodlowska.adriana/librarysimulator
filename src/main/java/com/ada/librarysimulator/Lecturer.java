package com.ada.librarysimulator;

public class Lecturer extends User {
    public Lecturer(String firstName, String lastName) {
        super(firstName, lastName, 'L');
    }
}
